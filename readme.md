# 一个 JS 实现的部分数据结构的包

## Stack(栈)

栈是一种特殊的列表，栈内的元素只能通过列表的一端访问，这一端称为栈顶。咖啡厅内的一摞盘子是现实世界中常见的栈的例子。只能从最上面取盘子，盘子洗净后，也只能摞在这一摞盘子的最上面。栈被称为一种后入先出`（LIFO，last-in-ﬁrst-out）`的数据结构。

### 创建栈

```typescript
import { Stack } from "gxc-js-data-structure";

// 不传参数为空栈
const stack1 = new Stack();
// 可传入一个数组转换为栈
const stack2 = new Stack([]);
```

### 添加一个新元素到栈顶

```typescript
const stack1 = new Stack();
stack1.push(5);
```

### 移除栈顶元素

```typescript
const stack1 = new Stack();
stack1.pop();
```

### 返回栈顶元素

```typescript
const stack1 = new Stack();
stack1.peek();
```

### 判断是否空栈

```typescript
const stack1 = new Stack();
stack1.isEmpty(); // true or false
```

### 返回栈里元素个数，类似数组的 length

```typescript
const stack1 = new Stack();
stack1.size;
```

### 将栈结构的内容以字符串的形式返回

```typescript
const stack1 = new Stack();
stack1.toString();
```

## Queue(队列)

### 创建队列

```ts
import { Queue } from "gxc-js-data-structure";
const queue1 = new Queue();
// 或者传入初始数组
const queue2 = new Queue([]);
```

### 向队列尾部添加一个或多个新的项

```ts
import { Queue } from "gxc-js-data-structure";
const queue = new Queue();
queue.enqueue(item);
```

### 移除队列的第一个项（最前面），并返回被移除的元素

```ts
import { Queue } from "gxc-js-data-structure";
const queue = new Queue();
queue.dequeue();
```

### 返回队列中第一个元素——最先被添加的项,不改变队列，类似栈的 peek

```ts
import { Queue } from "gxc-js-data-structure";
const queue = new Queue();
queue.front();
```

### 判断是否空队列

```typescript
const queue = new Queue();
queue.isEmpty(); // true or false
```

### 返回队列元素个数

```typescript
const queue = new Queue();
queue.size; // true or false
```

### 将队列中的内容转成字符串形式

```typescript
const queue = new Queue();
queue.toString(); // true or false
```
## LinkedList(链表)
### 创建链表
```ts
import { LinkedList } from "gxc-js-data-structure";
const lList = new LinkedList();
```
### 链表节点个数
```ts
lList.size
```
### 判断是否为空链表
```ts
lList.isEmpty
```
### 指定位置插入节点
```ts
lList.insert(item, index) // index为插入位置，默认为链表尾部
```
### 指定位置节点
```ts
lList.update(item, index) // index为指定节点位置
```
### 获取指定位置的节点
```ts
lList.find(index) // index为需要获取节点的位置
```