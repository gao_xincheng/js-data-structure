import { NodeItem } from "./LinkedList";
// 双向链表

class DoubleNodeItem extends NodeItem {
  prev: NodeItem | null
  constructor(data: any){
    super(data)
    this.prev = null
  }
}

export class DoubleLinkedList {
  head: DoubleNodeItem | null;
  constructor() {
    this.head = null;
  }
}
