export class NodeItem {
  data: any;
  next: NodeItem | null;
  constructor(data: any) {
    this.data = data;
    this.next = null;
  }
  get [Symbol.toStringTag]() {
    return "NodeItem";
  }
}

export class LinkedList {
  head: NodeItem | null;
  constructor() {
    this.head = null;
  }
  get [Symbol.toStringTag]() {
    return "LinkedList";
  }
  /**
   * 获取链表节点个数
   */
  get size() {
    let count = 0;
    let current = this.head;
    if (this.head === null) return 0;
    while (current) {
      count++;
      current = current.next;
    }
    return count;
  }
  /**
   * 判断是否为空链表
   */
  get isEmpty() {
    return this.size === 0;
  }
  /**
   * 指定位置插入节点
   * @param item 节点
   * @param index 节点位置
   */
  insert(item: any, index: number = this.size) {
    this.judgedIndex(index);
    const nodeItem = new NodeItem(item);
    if (index === 0) {
      const next = this.head?.next || null;
      this.head = nodeItem;
      nodeItem.next = next;
    } else {
      let prev = this.getPreviousItem(index);
      nodeItem.next = prev.next?.next || null;
      prev.next = nodeItem;
    }
  }
  /**
   * 更新指定位置的节点
   * @param item 节点
   * @param index 节点位置
   */
  update(item: any, index: number) {
    const nodeItem = this.findNodeItem(index);
    if (nodeItem) {
      nodeItem.data = item;
      return nodeItem.data;
    } else {
      throw new Error("update failed, there is no node at this location ");
    }
  }
  /**
   * 获取指定位置的节点数据
   * @param index 节点位置
   */
  find(index: number) {
    const nodeItem = this.findNodeItem(index);
    return nodeItem?.data;
  }

  /**
   * 删除指定位置的节点
   * @param index 节点位置
   */
  removeAt(index: number) {
    this.judgedIndex(index);
    const nodeItem = this.findNodeItem(index);

    if (nodeItem) {
      if (index === 0) {
        this.head = nodeItem.next;
        return nodeItem.data;
      }
      const prevItem = this.getPreviousItem(index);
      prevItem.next = nodeItem.next;
    }
  }

  /**
   * 删除链表中满足提供的测试函数的第一个节点
   * @param fn
   */
  remove(fn: (node: any) => Boolean) {
    let currentNode = this.head;
    let currentIndex = 0;
    while (currentNode) {
      if (fn(currentNode)) {
        const prevNode = this.getPreviousItem(currentIndex);
        prevNode.next = currentNode.next;
        return currentNode.data;
      }
      currentIndex++;
      currentNode = currentNode.next;
    }
  }

  /**
   * 获取指定位置的节点
   * @param index 节点位置
   */
  private findNodeItem(index: number) {
    let currentIndex = 0;
    let current = this.head;
    while (current) {
      if (currentIndex === index) {
        return current;
      }
      current = current.next;
      currentIndex++;
    }
  }

  /**
   * 判断输入的index是否合法
   * @param index 索引
   */
  private judgedIndex(index: number) {
    if (index < 0 || index > this.size) {
      throw new Error(
        `index must be a non-negative number no greater than length! at judgedIndex(${index})`
      );
    }
  }

  /**
   * 获得当前位置前一个节点（index不为0）
   * @param index 索引
   */
  private getPreviousItem(index: number) {
    if (index === 0)
      throw new Error("The first node has no previous node at getPreviousItem");
    let current = this.head;
    let currentIndex = 0;
    while (current) {
      if (currentIndex === index - 1) {
        break;
      }
      current = current.next;
      currentIndex++;
    }
    return current as NodeItem;
  }
}
