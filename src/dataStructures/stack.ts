export class Stack {
  private _items!: Array<any>;
  constructor(items: Array<any> = []) {
    Object.defineProperty(this, "_items", {
      enumerable: false,
      configurable: false,
      writable: false,
      value: items,
    });
  }
  get [Symbol.toStringTag]() {
    return "Stack";
  }
  //添加一个新元素到栈顶
  push(element: any) {
    this._items.unshift(element);
    return this._items;
  }
  //移除栈顶元素
  pop() {
    return this._items.shift();
  }
  //返回栈顶元素
  peek() {
    return this._items[0];
  }
  //如果栈里没有任何元素就返回true，否则返回false
  isEmpty() {
    return this._items.length === 0;
  }
  //返回栈里元素个数，类似数组的length
  get size() {
    return this._items.length;
  }
  //将栈结构的内容以字符串的形式返回
  toString() {
    return this._items.toString();
  }
}
