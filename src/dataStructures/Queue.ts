export class Queue {
  private _items!: Array<any>;
  constructor(items: Array<any> = []) {
    Object.defineProperty(this, "_items", {
      enumerable: false,
      configurable: false,
      writable: false,
      value: items,
    });
  }
  get [Symbol.toStringTag]() {
    return "Queue";
  }
  // 向队列尾部添加一个或多个新的项
  enqueue(...args: any[]) {
    this._items.push(...args);
  }
  // 移除队列的第一个项（最前面），并返回被移除的元素
  dequeue() {
    return this._items.shift();
  }
  // 返回队列中第一个元素——最先被添加的项,不改变队列，，类似栈的peek
  front() {
    return this._items[0];
  }
  //  如果队列中不包含任何元素，返回true
  isEmpty() {
    return this._items.length === 0;
  }
  // 返回队列元素个数
  get size() {
    return this._items.length;
  }
  // 将队列中的内容转成字符串形式
  toString() {
    return this._items.join(" ");
  }
}
