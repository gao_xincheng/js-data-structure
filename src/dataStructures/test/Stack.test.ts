import { Stack } from "../Stack";

test("Stack`s methods:", () => {
  const testStack = new Stack([1, 2, 3]);
  expect(testStack.size).toBe(3);
  expect(testStack.isEmpty()).toBe(false);
});

test("Stack`s push", () => {
  const testStack = new Stack([1, 2, 3]);
  expect(testStack.push(6).length).toBe(4);
});
