import resolve from "@rollup/plugin-node-resolve"; // 解析引用插件
import commonjs from "@rollup/plugin-commonjs";
import ts from "rollup-plugin-typescript2"; // TS解析插件
// import { babel } from "@rollup/plugin-babel";
import { terser } from "rollup-plugin-terser";
import path from "path";
import packageJSON from "./package.json";

const getPath = (_path) => path.resolve(__dirname, _path);

export default {
  input: "src/index.ts", // 打包入口
  output: [
    {
      file: packageJSON.main,
      format: "umd", // umd是兼容amd/cjs/iife的通用打包格式，适合浏览器
      name: "dataStructures",
    },
    {
      file: 'lib/index.min.js',
      format: "umd", // umd是兼容amd/cjs/iife的通用打包格式，适合浏览器
      name: "dataStructures",
      plugins: [terser()]
    },
    {
      file: packageJSON.module, // es6模块
      format: "esm",
      name: "dataStructures",
    },
    {
      file: 'lib/index.esm.min.js', // es6模块
      format: "esm",
      name: "dataStructures",
      plugins: [terser()]
    },
  ],
  plugins: [
    // 打包插件
   
    ts({
      tsconfig: getPath("./tsconfig.json"),
    }),
    // babel({
    //   babelHelpers: "runtime",
    //   exclude: "node_modules/**",
    //   include: "src/**",
    //   extensions: [".js", ".jsx", ".es6", ".es", ".mjs", ".ts", ".tsx"],
    // }),
    resolve(), // 查找和打包node_modules中的第三方模块
    commonjs(),
  ],
};
